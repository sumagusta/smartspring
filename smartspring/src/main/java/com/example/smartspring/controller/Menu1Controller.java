package com.example.smartspring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Menu1Controller {

	@RequestMapping(value="/menu1")
	public String menu1() {
		String menu1="/menu/menu1";
		return menu1;
	}
}
