package com.example.smartspring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_PASIEN")
public class PasienModel {
	
	@Id
	@Column(name="NOMOR")
	private String noPasien;
	
	@Column(name="NAMA")
	private String namaPasien;
	
	@Column(name="GENDER")
	private String gender;
	
	@Column(name="KATEGORY")
	private String kategory;
	
	@Column(name="BIAYA")
	private int biaya;
	
	@Column(name="TANGGAL")
	private Date tanggalLahhir;

	public String getNoPasien() {
		return noPasien;
	}

	public void setNoPasien(String noPasien) {
		this.noPasien = noPasien;
	}

	public String getNamaPasien() {
		return namaPasien;
	}

	public void setNamaPasien(String namaPasien) {
		this.namaPasien = namaPasien;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getKategory() {
		return kategory;
	}

	public void setKategory(String kategory) {
		this.kategory = kategory;
	}

	public int getBiaya() {
		return biaya;
	}

	public void setBiaya(int biaya) {
		this.biaya = biaya;
	}

	public Date getTanggalLahhir() {
		return tanggalLahhir;
	}

	public void setTanggalLahhir(Date tanggalLahhir) {
		this.tanggalLahhir = tanggalLahhir;
	}

}
