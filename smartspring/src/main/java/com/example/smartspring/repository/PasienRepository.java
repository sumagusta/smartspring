package com.example.smartspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.smartspring.model.PasienModel;

/*JpaRepository<T, ID> = T = Tabelnya, ID = primary key dari tabel*/
public interface PasienRepository extends JpaRepository<PasienModel, String>{

	/*
	 * @Query("select p from PasienModel p") List<PasienModel> repoRead();
	 */
	
	@Query("select p from PasienModel p where p.namaPasien like %?1% ")
	List<PasienModel> searchNamaPasien(String namaPasien);
	
	@Query("select p from PasienModel p where p.noPasien = ?1")
	PasienModel searchNoPasien(String noPasien);
	
	
}
